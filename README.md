gitea-bash
=========

Interact with the [GITEA](https://gitea.io/) API from bash.

Create repositories and manage webooks.

Setup:
------

Set the following environment variables:

* GITEA_ROOT_URL - The url of the gitea server (IE: http://try.gitea.io)
* GITEA_TOKEN - Access token obtained from gitea (http://try.gitea.io/user/settings/applications)

Usage:
------

```
    ./gitea command [arguments]
```

For a list of currently supported commands type:
```
    ./gitea help
```

For more information about a command use:
```
	./gitea help [command]"
```
